  function storeValues()
  {
    var fname = document.getElementById("commentField").value;
    setCookie("field1", fname);
    return true;
  }

  var today = new Date();
  var expiry = new Date(today.getTime() + 1 * 24 * 3600 * 1000); // plus 1 day

  function setCookie(name, value)
  {
    document.cookie= name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
  }