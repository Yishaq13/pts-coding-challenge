import socketserver
import http.server


class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.path = 'index.html'

        if self.path == '/get-text':
            self.path = 'get-text.html'
        return http.server.SimpleHTTPRequestHandler.do_GET(self)


# Create an object of the above class
handler_object = MyHttpRequestHandler

PORT = 9998
my_server = socketserver.TCPServer(("", PORT), handler_object)

# Star the server
my_server.serve_forever()